use std::error::Error;
use std::io;
use std::io::Write;

fn main() -> Result<(), Box<dyn Error>>{
    let entfernung_km = f32_eingeben("Entfernung / km")?;
    let benzin_liter_pro_100km = f32_eingeben("Benzin / L/100km")?;
    println!("Entfernung: {entfernung_km} km");
    println!("Benzin: {benzin_liter_pro_100km} L/100km");
    println!("Gesamtverbrauch: {} L", entfernung_km * benzin_liter_pro_100km / 100.0);
    Ok(())
}

fn f32_eingeben(frage: &str) -> Result<f32, Box<dyn Error>> {
    print!("{}: ", frage);
    io::stdout().flush()?;
    let mut eingabe = String::new();
    io::stdin().read_line(&mut eingabe)?;
    let nummer: f32 = eingabe.trim().parse()?;
    Ok(nummer)
}
